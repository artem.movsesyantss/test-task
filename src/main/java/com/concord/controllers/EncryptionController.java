package com.concord.controllers;

import com.concord.dto.Person;
import com.concord.dto.PersonRequest;
import com.concord.utils.CryptoUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class EncryptionController {
    ObjectMapper mapper = new ObjectMapper();

    @PostMapping("/getPerson")
    public Person getPerson(@RequestBody PersonRequest request) {
        Person response = request.getId() == 1 ? new Person("Test Testov") : null;

        try {
            String requestJson = mapper.writeValueAsString(request);
            log.info("Request - {}", requestJson);

            String responseJson = mapper.writeValueAsString(response);
            log.info("Response - {}", responseJson);

            String encryptedRequest = CryptoUtils.encrypt(requestJson);
            log.info("Encrypted Request - {}", encryptedRequest);
            log.info("Decrypted Request - {}", CryptoUtils.decrypt(encryptedRequest));

            String encryptedResponse = CryptoUtils.encrypt(responseJson);
            log.info("Encrypted Response - {}", encryptedResponse);
            log.info("Decrypted Response - {}", CryptoUtils.decrypt(encryptedResponse));
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException ", e);
        }

        return response;
    }

}
